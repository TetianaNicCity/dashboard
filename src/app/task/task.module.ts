import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskCalendarComponent } from './task-calendar/task-calendar.component';
import { TaskChangeComponent } from './task-change/task-change.component';
import { TaskComponent } from './task.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskListItemComponent } from './task-list/task-list-item/task-list-item.component';
import { MaterialAppModule } from '../ngmaterial.module';

@NgModule({
  declarations: [
    TaskComponent,
    TaskDetailComponent,
    TaskCalendarComponent,
    TaskChangeComponent,
    TaskListComponent,
    TaskListItemComponent
  ],
  imports: [
    CommonModule,
    MaterialAppModule
  ],
  exports: [
    TaskComponent
  ]
})
export class TaskModule { }
