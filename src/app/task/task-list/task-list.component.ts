import { Component, OnInit } from '@angular/core';
import { Task } from '../../shared/model/task.model';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit {
  tasks: Task[];

  constructor() { }

  ngOnInit() {
    this.tasks = [ new Task('First task', 'My job', 'description first task',
    'Mon Nov 25 2019 10:15:56 GMT+0200',
     'Mon Nov 25 2019 10:15:56 GMT+0200',
     'Mon Nov 25 2019 10:15:56 GMT+0200',
     'In process' ),
      new Task('Second task', 'My weekends', 'description first task',
     'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Done' ),
      new Task('Third task', 'My weekends', 'description first task',
     'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Mon Nov 25 2019 10:15:56 GMT+0200',
      'Failed' )
     ];
  }
}
