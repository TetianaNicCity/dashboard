import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../../../shared/model/task.model';


@Component({
  selector: 'app-task-item',
  templateUrl: './task-list-item.component.html',
  styleUrls: ['./task-list-item.component.scss']
})

export class TaskListItemComponent implements OnInit {
  @Input() task: Task;
  constructor() { }

  ngOnInit() {
    console.log(this.task.status);
  }
  checkStatus() {
    if (this.task.status === 'Done') {
      return 'blue';
    }
    if (this.task.status === 'Failed') {
      return 'red';
    }
    if (this.task.status === 'In process') {
      return 'green';
    }
  }
}
