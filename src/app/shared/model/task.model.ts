export class Task {
    constructor(
        public title: string,
        public group: string,
        public descr: string,
        public inTime: string,
        public startTime: string,
        public endTime: string,
        public status: string
        ) {}
    }
